﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Personal_data
{
    public partial class Form1 : Form
    {

        public List <Panel> PersonalList = new List<Panel>();

        public Data data = new Data();

        public Form1()
        {
            InitializeComponent();
            PersonalList.Add(panel1);
            PersonalList.Add(panel2);
            PersonalList.Add(panel3);
            PersonalList.Add(panel4);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
            foreach (var item in PersonalList)
            {
                if (item.Visible && PersonalList.IndexOf(item) != PersonalList.Count - 1)
                {
                    item.Visible = false;
                    PersonalList[PersonalList.IndexOf(item) + 1].Visible = true;
                    break;
                }
                if (PersonalList.IndexOf(item) == PersonalList.Count - 1)
                {
                    item.Visible = false;
                    panel5.Visible = true;
                    textBox5.Text = data.ToString();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (var item in PersonalList.Where(item => item.Visible && PersonalList.IndexOf(item) != 0))
            {
                if (PersonalList.IndexOf(item) == 1) button1.Enabled = false;
                item.Visible = false;
                PersonalList[PersonalList.IndexOf(item) - 1].Visible = true;
                break;
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            data.EnterPhoneNumber(textBox4.Text);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            data.EnterAddress(textBox3.Text);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            data.EnterFirstName(textBox1.Text);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            data.EnterLastName(textBox2.Text);
        }
    }
}
