﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Personal_data
{
    public class Data
    {
        string _firstName;
        string _lastName;
        string _address;
        string _phoneNumber;

        public override string ToString()
        {
            return "Imię: " + _firstName + "\r\nNazwisko: " +_lastName+ "\r\nAdres: " + _address + "\r\nNumer telefonu: " +
                   _phoneNumber;
        }

        public void EnterFirstName(string firstName)
        {
            _firstName = firstName;
        }

        public void EnterLastName(string lastName)
        {
            _lastName = lastName;
        }
        public void EnterAddress(string address)
        {
            _address = address;
        }
        public void EnterPhoneNumber(string number)
        {
            _phoneNumber = number;
        }

    }
}
